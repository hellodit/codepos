var ajaxCat = createCategories();

function ajaxCategories( alias, sel, display) {
    var url = APP_URL + "/categories/ajax?alias=" + alias + (!sel ? "" : "&selected=" + sel) + (!display ? "" : "&display=" + display);
    ajaxCat.onreadystatechange = stateChangedCategories;
    ajaxCat.open("GET", url, true);
    ajaxCat.send(null);
}

function stateChangedCategories() {
    var merge;
    if (ajaxCat.readyState == 4) {
        merge = ajaxCat.responseText;
        var xz = JSON.parse(merge);
        if (merge.length >= 0) {
            document.getElementById(xz.id + "-categories").innerHTML = xz.data
        } else {
            document.getElementById(xz.id + "-categories").value = "<option selected>Pilih Kategori</option>";
        }
    }
}

function createCategories() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
}
