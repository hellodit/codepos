var ajaxCat = createIngredients();

function ajaxIngredients( alias, sel, display) {
    var url = APP_URL + "/ingredients/ajax?alias=" + alias + (!sel ? "" : "&selected=" + sel) + (!display ? "" : "&display=" + display);
    ajaxCat.onreadystatechange = stateChangedIngredients;
    ajaxCat.open("GET", url, true);
    ajaxCat.send(null);
}

function stateChangedIngredients() {
    var merge;
    if (ajaxCat.readyState == 4) {
        merge = ajaxCat.responseText;
        var xz = JSON.parse(merge);
        if (merge.length >= 0) {
            document.getElementById(xz.id + "-ingredients").innerHTML = xz.data
        } else {
            document.getElementById(xz.id + "-ingredients").value = "<option selected>Pilih Kategori</option>";
        }
    }
}

function createIngredients() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
}
