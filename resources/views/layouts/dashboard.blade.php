<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title> @yield('title') - codepos</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{asset('stisla/assets/modules/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('stisla/assets/modules/fontawesome/css/all.min.css')}}">
  <!-- Template CSS -->
  <link rel="stylesheet" href="{{asset('stisla/assets/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('stisla/assets/css/components.css')}}">
  <link rel="stylesheet" href="{{asset('stisla/assets/modules/izitoast/css/iziToast.css')}}">
  @yield('customcss')
</head>

<body class="layout-2">
  <div id="app">
    <div class="main-wrapper">
        @include('layouts.parts.navbar')
      <div class="main-sidebar">
        @include('layouts.parts.sidebar')
      </div>
      <!-- Main Content -->
      <div class="main-content">
          @yield('content')
      </div>
        @include('layouts.parts.footer')
    </div>
  </div>
  
  @if (!empty($modal))
     @include('layouts.parts.modal')
  @endif

  <!-- General JS Scripts -->
  <script src="{{asset('stisla/assets/modules/jquery.min.js')}}"></script>
  <script src="{{asset('stisla/assets/modules/popper.js')}}"></script>
  <script src="{{asset('stisla/assets/modules/tooltip.js')}}"></script>
  <script src="{{asset('stisla/assets/modules/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('stisla/assets/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
  <script src="{{asset('stisla/assets/modules/moment.min.js')}}"></script>
  <script src="{{asset('stisla/assets/js/stisla.js')}}"></script>
  <!-- Template JS File -->
  <script src="{{asset('stisla/assets/js/scripts.js')}}"></script>
  <script src="{{asset('stisla/assets/js/custom.js')}}"></script>
  <!-- JS Libraies -->
  <script src="{{asset('stisla/assets/modules/sticky-kit.js')}}"></script>
  <script src="{{asset('stisla/assets/modules/izitoast/js/iziToast.js')}}"></script>
  @yield('customjs')
</body>
</html>