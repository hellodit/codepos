<aside id="sidebar-wrapper">
    <div class="sidebar-brand sidebar-gone-show"><a href="index.html">codepos</a></div>
    <ul class="sidebar-menu">
        <li class="menu-header">Dashboard</li>
        <li><a href="{{url('home')}}" class="nav-link "><i class="fas fa-fire"></i><span>Dashboard</span></a></li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-arrow-circle-down"></i> <span>Master Data</span></a>
            <ul class="dropdown-menu">
                <li><a href="{{url('users')}}" class="nav-link"><i class="fas fa-user"></i> <span>Master User</span></a></li>
                <li><a href="{{url('categories')}}" class="nav-link"><i class="fas fa-tag"></i> <span>Master Categories</span></a></li>
                <li><a href="{{url('ingredients')}}" class="nav-link"><i class="fas fa-atlas"></i> <span>Master Igridient</span></a></li>
                <li><a href="{{url('menus')}}" class="nav-link"><i class="fas fa-barcode"></i> <span>Master Menu</span></a></li>
                <li><a href="{{url('menus')}}" class="nav-link"><i class="fas fa-percent"></i> <span>Master Diskon</span></a></li>
            </ul>
        </li>

        <li class="menu-header">Manage</li>
        <li><a href="{{url('cashier')}}" class="nav-link"><i class="fas fa-calculator"></i><span>Cashier</span></a></li>
        <li><a href="{{url('daily_balance')}}" class="nav-link"><i class="fas fa-monument"></i> <span>Saldo / Modal Buka</span></a></li>
        <li><a href="{{url('spending')}}" class="nav-link"><i class="fas fa-book"></i> <span>Peng. Tidak Terduga</span></a></li>
        <li><a href="{{url('buying')}}" class="nav-link"><i class="fas fa-book"></i> <span>Pembelian Bahan Menu</span></a></li>

    </ul>

    <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
        <a href="{{url('settings')}}" class="btn btn-primary btn-lg btn-block btn-icon-split">
            <i class="fas fa-rocket"></i> Setting Apps
        </a>
    </div>
</aside>