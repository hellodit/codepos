@extends('layouts.dashboard')
@section('title','Master User')
@section('content')
    <div class="section">
        <div class="section-header">
            <h1>Master user</h1>
            @include('home.parts.breadcrumb')
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div id="form-display"></div>
        </div>
        <div class="col-md-8">
            <div id="data-display"></div>
        </div>
    </div>
@endsection

@section('customcss')
    <link rel="stylesheet" href="{{asset('stisla/assets/modules/select2/dist/css/select2.css')}}">
    <link rel="stylesheet" href="{{asset('stisla/assets/modules/file-input/css/fileinput.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.css">

    <style>
    .kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
        margin: 0;
        padding: 0;
        border: none;
        box-shadow: none;
        text-align: center;
    }
    .kv-avatar {
        display: inline-block;
    }
    .kv-avatar .file-input {
        display: table-cell;
        width: 213px;
    }
    .kv-reqd {
        color: red;
        font-family: monospace;
        font-weight: normal;
    }
    </style>    
@endsection

@section('customjs')
<script src="{{asset('stisla/assets/modules/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('stisla/assets/modules/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('stisla/assets/modules/file-input/js/fileinput.js')}}"></script>
<script src="{{asset('stisla/assets/modules/file-input/themes/fa/theme.min.js')}}"></script>


    <script>
        actControl("form");
        actControl("data");
        function actControl(x, y, z) {
            if (x == "data") {
                $("#data-display").load('{{url("users/data")}}');
            } else if (x == "form") {
                $("#form-display").load('{{url("users/form")}}' +"/"+ (!y ? "" : y ));
            }else if( x == "detail"){
                $("#modal-setup").attr("class", "modal-dialog modal-md");
                $("#myLargeModalBody").load('{{url("users")}}' +"/"+ (!y ? "" : y ));
            }else if (x == "delete") {
                var csrf_token = $('meta[name="csrf-token"]').attr('content');
                swal({
                        title: 'Are you sure?',
                        text: 'Once deleted, you will not be able to recover this imaginary file!',
                        icon: 'warning',
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: 'users/' + y,
                                type : "POST",
                                data : {'_method' : 'DELETE', '_token' : csrf_token},
                                dataType: "JSON",
                                success:function(json){
                                    actControl("data");
                                    if(json.status == 0){
                                        iziToast.error({
                                            title: 'Error',
                                            message: json.pesan,
                                            position: 'bottomRight'
                                        });      
                                    }else{
                                        iziToast.success({
                                            title: 'Success',
                                            message: json.pesan,
                                            position: 'bottomRight'
                                        });
                                    }
                                }
                            });
                        } else {
                            swal('Your imaginary file is safe!');
                        }
                    });
            }
        }
    </script>
@endsection