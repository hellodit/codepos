<div class="card profile-widget">
    <div class="profile-widget-header">
        <img alt="image" src="{{!empty($data["user"]) ? asset('storage/avatar/'.$data["user"]->photo) : null }}" class="rounded-circle profile-widget-picture">
    </div>
    <div class="profile-widget-description">
        <div class="profile-widget-name">{{$data['user']->first_name.' '.$data['user']->last_name}} <div class="text-muted d-inline font-weight-normal">
                <div class="slash"></div> {{$data['user']->level == 1 ? "Admin":  "Kasir" }}
            </div>
        </div>
        Ujang maman is a superhero name in <b>Indonesia</b>, especially in my family. He is not a fictional character
        but an original hero in my family, a hero for his children and for his wife. So, I use the name as a user in
        this template. Not a tribute, I'm just bored with <b>'John Doe'</b>.
    </div>
</div>
<script type="text/javascript">
    $("#myLargeModalLabel").html("<i class='fas fa-user m-r-10'></i> Detail User");
</script>