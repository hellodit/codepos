<div class="card">
    <div class="card-header">
        <h3 class="title">{{$data['act']}}</h3>
    </div>
    <div class="card-body">
        <form method="post" id="forms" enctype="multipart/form-data">
            {{ csrf_field() }}
            @if (!empty($data['user']))
                <input type="hidden" name="id" value="{{$data['user']->id}}"/>
            @endif
            
            <div class="form-group text-center">
                <div class="kv-avatar">
                    <div class="file-loading">
                        <input id="user-avatar" name="avatar" type="file">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="labelUserName">Username*</label>
                <input type="text" class="form-control" name="username" value="{{!empty($data['user']) ? $data['user']->username : null}}">
            </div>

            <div class="form-group">
                <label for="labelEmail">Email*</label>
                <input type="text" class="form-control" name="email" value="{{!empty($data['user']) ? $data['user']->email : null}}">
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="labelFirstName">First Name*</label>
                    <input type="text" class="form-control" name="first_name" id="firstname" placeholder="First Name" value="{{!empty($data['user']) ? $data['user']->first_name : null}}">
                </div>

                <div class="form-group col-md-6">
                    <label for="inputPassword4">Last Name*</label>
                    <input type="text" class="form-control" name="last_name" id="lastname" placeholder="Last Name" value="{{!empty($data['user']) ? $data['user']->last_name : null}}">
                </div>
            </div>

            <div class="form-group">
                <label for="labelLevel">Level*</label>
                <select class="form-control select2" id="level" name="level">
                        <option value=""></option>
                    @foreach (array("Admin","Cassier") as $key => $item)
                        <option value="{{$key+1}}" {{empty($data['user']) ? null : ($data['user']->level == $key+1 ? "selected" : null)}} >{{$item}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="labelLevel">Shift*</label>
                <select class="form-control select2" id="shift" name="shift">
                    <option value=""></option>
                    @foreach (array("Pagi","Siang") as $keyShift => $itemShift)
                        <option value="{{$keyShift+1}}" {{empty($data['user']) ? null : ($data['user']->shift_id == $keyShift+1 ? "selected" : null)}}>{{$itemShift}}</option>
                    @endforeach
                </select>
            </div>
            <div class="row m-t-10">
                <div class="col-lg-6">
                    <button type="button" onclick="actControl('form')" class="btn btn-block btn-danger"><i class="fa fa-undo m-r-10"></i>Reset</button>
                </div>
                <div class="col-lg-6">
                    <button type="submit" class="btn btn-block btn-info"><i class="fa fa-save m-r-10"></i>Simpan</button>
                </div>
            </div>
        </form>
    </div>
    <div class="card-footer">
        <small class="text-muted">Default password : 123456</small>
    </div>
</div>

<script type="text/javascript">
    $(".select2").select2({
        placeholder: "Select a state",
        theme: "bootstrap"
    });

    $("#forms").submit(function (e) {
        e.preventDefault();
        let that = $(e.target)
        var ajaxData = new FormData(that.get(0));        
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url : "{{$data['url'] }}",
            type: "POST",
            data: ajaxData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (json) {
                if (json.status == 0) {
                    iziToast.error({
                        title: 'Error',
                        message: json.pesan,
                        position: 'topRight'
                    });                
                }else{
                    iziToast.success({
                        title: 'Success',
                        message: json.pesan,
                        position: 'topRight'
                    });
                }
                actControl("data");
                actControl("form");
            },
            error: function (){
                iziToast.error({
                        title: 'Error',
                        message: 'terjadi kesalahan sistem !!',
                        position: 'topRight'
                });  
            }
        });
        return false;
    });

    $("#user-avatar").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        showBrowse: false,
        browseOnZoneClick: true,
        removeClass: 'btn btn-danger',
        removeLabel: 'Remove Image',        
        removeIcon: '<i class="fas fa-trash"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-2',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img class="card-img-top img-responsive" src="{{!empty($data["user"]) ? asset("storage/avatar/".$data["user"]->photo) : null }}" alt="Foto Profil">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });

</script>