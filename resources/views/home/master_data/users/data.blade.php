<div class="card">
    <div class="card-header">
        <h4>Table User</h4>
    </div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-striped table-md">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Terdaftar Pada</th>
                        <th>Jabatan</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if (!$users->isEmpty())
                        @foreach ($users as $nom => $user)
                        <tr>
                            <td>{{$nom+1}}</td>
                            <td>{{$user->first_name.' '.$user->last_name}}</td>
                            <td>{{indo_date($user->created_at,true)}}</td>
                            <td>
                                @if ($user->level == 1)
                                    Admin
                                @else
                                    Kasir
                                @endif
                            </td>
                            <td>
                                @if ($user->status == 1)
                                    <div class="badge badge-success">Active</div>
                                @else
                                    <div class="badge badge-danger">Inactive</div>
                                @endif
                            </td>
                            <td>
                                <a href="#" class="btn btn-primary" role="button" data-toggle="modal" data-target=".bs-modal-lg"
                                    style="cursor:pointer" onclick="actControl('detail','{{$user->id}}')"><i class="fas fa-eye"></i></a>
                                <a href="#" class="btn btn-info" onclick="actControl('form','{{$user->id}}')"><i class="fas fa-edit"></i></a>
                                <a href="#" class="btn btn-danger" onclick="actControl('delete','{{$user->id}}')"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <td colspan="6">Belum ada data ditambahkan</td>
                    @endif
                    
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer text-right">
        <nav class="d-inline-block">
            <ul class="pagination mb-0">
                {{-- <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1"><i class="fas fa-chevron-left"></i></a>
                </li>
                <li class="page-item active"><a class="page-link" href="#">1 <span class="sr-only">(current)</span></a></li>
                <li class="page-item">
                    <a class="page-link" href="#">2</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#"><i class="fas fa-chevron-right"></i></a>
                </li> --}}
                {{ $users->links() }}

            </ul>
        </nav>
    </div>
</div>