@extends('layouts.dashboard')
@section('title')
Menu
@endsection
@section('customcss')
    <link rel="stylesheet" href="{{asset('stisla/assets/modules/select2/dist/css/select2.css')}}">
    <link rel="stylesheet" href="{{asset('stisla/assets/modules/select2/dist/css/select2-bootstrap4.css')}}">
@endsection
@section('content')
<div class="section">
    <div class="section-header">
        <h1>Menu</h1>
        @include('home.parts.breadcrumb')
    </div>
</div>

<div class="row">
  <div class="col-md-3">
      <div id="filter-display"></div>
  </div>
  <div class="col-md-9">
      <div id="data-display"></div>
  </div>
</div>

@endsection

@section('customjs')
  <script src="{{asset('assets/modules/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
  <script src="{{ asset('js/ajax/categories.js')}}"></script>

    <script>

        actControl("data");
        actControl("filter");
        

        function actControl(x, y, z) {  
            if (x == "data") {
                $("#data-display").load('{{url("menus/data")}}');
            } else if( x== "filter"){
                $("#filter-display").load('{{url("menus/filter")}}');
            }
            else if( x == "form"){
                $("#modal-setup").attr("class", "modal-dialog modal-lg");
                $("#myLargeModalBody").load('{{url("menus/form")}}');
            }
        }
    </script>
@endsection