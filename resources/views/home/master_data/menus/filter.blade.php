<div class="card">
    <div class="card-header">
        <h4>Filter data</h4>
    </div>
    <div class="card-body">
        <form action="">
            <div class="form-group">
                <label>Kategori Menu</label>
                <input type="text" class="form-control datepicker">
            </div>
            
            <div class="form-group">
                <label>Harga Menu</label>
                <input type="text" class="form-control">
            </div>
            
            <div class="form-group">
                <label>Stok Menu</label>
                <input type="text" class="form-control">
            </div>

            <div class="form-group">
                <label>Date Range Picker</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <i class="fas fa-calendar"></i>
                        </div>
                    </div>
                    <input type="text" class="form-control daterange-cus">
                </div>
            </div>
            <hr>
            <div class="row m-t-10">
                <div class="col-lg-6">
                        <button type="submit" class="btn btn-block btn-info"><i class="fa fa-save m-r-10"></i>Simpan</button>
                </div>
                <div class="col-lg-6">
                        <button type="button" onclick="actControl('form')" class="btn btn-block btn-danger"><i class="fa fa-undo m-r-10"></i>Reset</button>
                </div>
            </div>
        </form>
    </div>
</div>