<div class="card">
    <div class="card-header">
            <h4>List Menu</h4>
            <a href="menus/form-menu" class="btn btn-primary float-left" role="button" data-toggle="modal" data-target=".bs-modal-lg" style="cursor:pointer" onclick="actControl('form')">Tambah data</a>
    </div>

    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-striped table-md">
                <tbody>
                    <tr>
                        <th>#</th>
                        <th>Kode Nama</th>
                        <th>Nama Menu</th>
                        <th>Harga</th>
                        <th>Kategori</th>
                        <th>Status</th>
                        <th>Bahan</th>
                        <th>Action</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>LX</td>
                        <td>Luxury Lassagna</td>
                        <td>50.000</td>
                        <td>Makanan</td>
                        <td>
                            <div class="badge badge-success">Tersedia</div>
                        </td>
                        <td>Minyak, Roti, Asu</td>
                        <td>
                            <a href="#" class="btn btn-info"><i class="fas fa-edit"></i></a>
                            <a href="#" class="btn btn-danger" onclick="actControl('delete')"><i class="fas fa-trash"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer">
        <p class="text-muted">Ini adalah catatan untuk balik</p>
    </div>
</div>