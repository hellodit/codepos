<div class="card">
    <div class="card-header">
        <h4>Tabel Bahan</h4>
    </div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-striped table-md">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Satuan</th>
                        <th>Harga</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if (!$ingredients->isEmpty())
                        @foreach ($ingredients as $nom => $ingredient)
                        <tr>
                            <td>{{$nom+1}}</td>
                            <td>{{$ingredient->name}}</td>
                            <td>{{ucfirst($ingredient->unit)}}</td>
                            <td>{{str_rp($ingredient->price)}}</td>
                            <td>
                                <a href="#" class="btn btn-info" onclick="actControl('form','{{$ingredient->id}}')"><i class="fas fa-edit"></i></a>
                                <a href="#" class="btn btn-danger" onclick="actControl('delete','{{$ingredient->id}}')"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <td colspan="6">Belum ada data ditambahkan</td>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer text-right">
        <nav class="d-inline-block">
            <ul class="pagination mb-0">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1"><i class="fas fa-chevron-left"></i></a>
                </li>
                <li class="page-item active"><a class="page-link" href="#">1 <span class="sr-only">(current)</span></a></li>
                <li class="page-item">
                    <a class="page-link" href="#">2</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#"><i class="fas fa-chevron-right"></i></a>
                </li>
            </ul>
        </nav>
    </div>
</div>