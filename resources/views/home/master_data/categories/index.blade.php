@extends('layouts.dashboard')
@section('title','Master Category')
@section('content')
    <div class="section">
        <div class="section-header">
            <h1>Master Category</h1>
            @include('home.parts.breadcrumb')
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div id="form-display"></div>
        </div>
        <div class="col-md-8">
            <div id="data-display"></div>
        </div>
    </div>
@endsection

@section('customcss')
    <link rel="stylesheet" href="{{asset('stisla/assets/modules/select2/dist/css/select2.css')}}">    
@endsection

@section('customjs')
<script src="{{asset('stisla/assets/modules/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('stisla/assets/modules/select2/dist/js/select2.full.min.js')}}"></script>

    <script>
        actControl("form");
        actControl("data");
        
        function actControl(x, y, z) {
            if (x == "data") {
                $("#data-display").load('{{url("categories/data")}}');
            } else if (x == "form") {
                $("#form-display").load('{{url("categories/form")}}' +"/"+ (!y ? "" : y ));
            } else if( x == "detail"){
                $("#modal-setup").attr("class", "modal-dialog modal-md");
            }else if (x == "delete") {
                var csrf_token = $('meta[name="csrf-token"]').attr('content');
                swal({
                        title: 'Are you sure?',
                        text: 'Once deleted, you will not be able to recover this imaginary file!',
                        icon: 'warning',
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: 'categories/' + y,
                                type : "POST",
                                data : {'_method' : 'DELETE', '_token' : csrf_token},
                                success:function(data){
                                    swal('Poof! Your imaginary file has been deleted!', {
                                    icon: 'success',
                                    });
                                    actControl("data");
                                }
                            });
                        } else {
                            swal('Your imaginary file is safe!');
                        }
                    });
            }
        }
    </script>
@endsection