<div class="card">
    <div class="card-header">
        <h3 class="title">{{$data['act']}}</h3>
    </div>
    <div class="card-body">
        <form method="post" id="forms" enctype="multipart/form-data">
            @csrf

            @if (!empty($data['category']))
                <input type="hidden" name="id" value="{{$data['category']->id}}"/>
            @endif

            <div class="form-group">
                <label for="labelName">Nama Kategori</label>
                <input type="text" class="form-control" name="name" value="{{!empty($data['category']) ? $data['category']->name : null}}">
            </div>
    
    
            <div class="form-group">
                <label for="labelType">Jenis Kategori</label>
                <select class="form-control select2" id="level" name="type">
                    <option value=""></option>
                    @foreach (array("Foods","Drinks","Other") as $keyCategory => $itemCategory)
                        <option value="{{$itemCategory}}" {{empty($data['category']) ? null : ($data['category']->type == $itemCategory ? "selected" : null)}}>{{$itemCategory}}</option>
                    @endforeach
                </select>
            </div>
    
            <div class="row m-t-10">
                    <div class="col-lg-6">
                        <button type="submit" class="btn btn-block btn-info"><i class="fa fa-save m-r-10"></i>Simpan</button>
                    </div>
                    <div class="col-lg-6">
                        <button type="button" onclick="actControl('form')" class="btn btn-block btn-danger"><i class="fa fa-undo m-r-10"></i>Reset</button>
                    </div>
            </div>
        </form>
    </div>
</div>
    
    <script type="text/javascript">
        $(".select2").select2({
            placeholder: "Select a state",
        });

        $("#forms").submit(function () {
            $.ajax({
                url: "{{$data['url'] }}",
                type: "{{$data['action']}}",
                data : $('#forms').serialize(),
                success: function (data) {
                    if ((data.errors)) {
                        iziToast.error({
                            title: 'Error',
                            message: 'Tidak dapat menyimpan data, perhatikan data bertanda bintang (*)!!',
                            position: 'topRight'
                        });                
                    }else{
                        iziToast.success({
                            title: 'Success',
                            message: 'Data Berhasil disimpan!',
                            position: 'topRight'
                        });
                    }
                    actControl("data");
                    actControl("form");
                },
                error: function (data){
                    iziToast.error({
                            title: 'Error',
                            message: 'Kesalahan tidak diketahui!!',
                            position: 'topRight'
                    });  
                }
            });
            return false;
        });
    </script>