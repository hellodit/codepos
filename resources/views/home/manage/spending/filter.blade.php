<div class="card">
    <div class="card-header">
        <h4>Filter data</h4>
    </div>
    <div class="card-body">

        <div class="form-group">
            <label>Date Picker</label>
            <input type="text" class="form-control">
        </div>

        <div class="form-group">
            <label for="">Penanggung Jawab</label>
            <select name="" id="" class="form-control select2 ">
                <option value="1">Bambang Sugimaan</option>
            </select>
        </div>

        <div class="form-group">
            <label for="">Kode Pengeluaran</label>
            <input type="text" class="form-control">
        </div>
    </div>
    <div class="row m-t-10">
        <div class="col-lg-6">
            <button type="submit" class="btn btn-block btn-info"><i class="fa fa-save m-r-10"></i>Simpan</button>
        </div>
        <div class="col-lg-6">
            <button type="button" onclick="actControl('form')" class="btn btn-block btn-danger"><i class="fa fa-undo m-r-10"></i>Reset</button>
        </div>
    </div>
</div>