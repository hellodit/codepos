<div class="modal-body">
        <form action="" method="post">
            <div class="row">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Ketarangan</th>
                                <th>Total</th>
                                <th><i class="fas fa-gear"></i></th>
                            </tr>
                        </thead>
                        <tbody class="detail_tambahan"></tbody>
                    </table>
                    <button class="btn btn-primary btn-block" type="button" onclick="addso_row(this)"><i class="fas fa-plus"></i></button>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
    </div>
    <script type="text/javascript">
        $("#myLargeModalLabel").html("<i class='fas fa-user m-r-10'></i> Tambah Pengeluaran Tidak Terduga Baru");
    </script>
    
      <script src="{{ asset('js/ajax/categories.js')}}"></script>
      <script src="{{ asset('js/ajax/ingredient.js')}}"></script>

    
      <script>
        addso_row();
    
        function addso_row() {
            $(".add-new");
            $(".row_out");
            for (var a = $(".row_tam").length + 1, b = $(".row_out").length + 1; b >= a; b--) {
                var d = b + 1;
                $(".nom_" + b).html(d);
                $(".nom_" + b).attr("class", "nom_" + d);
                $("#menu_select_" + b).attr("id", "menu_select_" + d);
                $("#btn_" + b).attr("onclick", "cut(this, " + d + ")");
                $("#menu_" + b).attr("onchange", "menusa(this," + d + ")");
                $("#menu_" + b).attr("id", "menu_" + d);
                $("#btn_" + b).attr("id", "btn_" + d);
                $(".total_" + b).attr("class", "text-right total_" + d);
                $("#total_add_" + b).attr("id", "total_add_" + d);
                $("#price_" + b).attr("onkeyup", "calcPay(this, " + d + ")");
                $("#price_" + b).attr("id", "price_" + d);
                $("#disc_" + b).attr("onkeyup", "calcPay(this, " + d + ")");
                $("#disc_" + b).attr("id", "disc_" + d);
                $("#compliment_" + b).attr("id", "compliment_" + d);
                $("#qty_" + b).attr("onkeyup", "calcPay(this, " + d + ")");
                $("#qty_" + b).attr("id", "qty_" + d);
                $("#note_" + b).attr("id", "note_" + d);
            }

            b = '<tr class="row_out row_tam add-new">' +
                '<td class="text-center"><p style="margin-top: 5px" class="nom_' + a + '">' + a +
                '</td><td id="menu_select_' + a + '"><input type="text" class="form-control" placeholder="Keperluan ..."><br>'+
                    '<div class="row"><div class="col-md-8"><input class="form-control" type="number" placeholder="Harga ..."></div>'+
                    '<div class="col-md-4"><input class="form-control" type"number" placeholder="Qty"></div></div>'+
                '<td class="text-right total_' + a + '" style="vertical-align:middle">Rp 0</td>' + 
                '<td class="text-right"><input type="hidden" name="compliment[]" id="compliment_' + a + '" value="1">'+
                    '<input type="hidden" name="total_add[]" id="total_add_' + a + '" value="" class="form-control"><center><button type="button" class="btn btn-sm btn-danger" id="btn_' + a + '" onclick="cut(this, ' + a + ')"><i class="fa fa-times"></i></button></center></td>' +
                '</tr>';
            $(".detail_tambahan").append(b);
        }
    
        function cut(a, b) {
            for (var d = $(".row_out").length, c = b - 1; c < d; c++) {
                var e = c + 1;
                $(".nom_" + e).html(c);
                $(".nom_" + e).attr("class", "nom_" + c);
                $("#menu_select_" + e).attr("id", "menu_select_" + c);
                $("#btn_" + e).attr("onclick", "cut(this, " + c + ")");
                $("#menu_" + e).attr("onchange", "menusa(this," + c + ")");
                $("#menu_" + e).attr("id", "menu_" + c);
                $("#btn_" + e).attr("id", "btn_" + c);
                $(".total_" + e).attr("class", "text-right total_" + c);
                $("#total_add_" + e).attr("id", "total_add_" + c);
                $("#price_" + e).attr("onkeyup", "calcPay(this, " + c + ")");
                $("#price_" + e).attr("id", "price_" + c);
                $("#disc_" + e).attr("onkeyup", "calcPay(this, " + c + ")");
                $("#disc_" + e).attr("id", "disc_" + c);
                $("#compliment_" + e).attr("id", "compliment_" + c);
                $("#qty_" + e).attr("onkeyup", "calcPay(this, " + c + ")");
                $("#qty_" + e).attr("id", "qty_" + c);
                $("#note_" + e).attr("id", "note_" + c);
            }
            d = a.parentNode.parentNode.parentNode;
            d.parentNode.removeChild(d);
            $("#orders_id").val();
            calcPayNew();
        }
        

      </script> 