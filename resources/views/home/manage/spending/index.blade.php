@extends('layouts.dashboard')
@section('title', 'Pengeluaran')
@section('customcss')
    <link rel="stylesheet" href="{{asset('stisla/assets/modules/select2/dist/css/select2.css')}}">
    <link rel="stylesheet" href="{{asset('stisla/assets/modules/select2/dist/css/select2-bootstrap4.css')}}">
@endsection

@section('content')
<div class="section">
    <div class="section-header">
        <h1>Pengeluaran Tidak Terduga</h1>
            @include('home.parts.breadcrumb')
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <div id="filter-display"></div>
    </div>
    <div class="col-md-9">
        <div id="data-display"></div>
    </div>
</div>

@endsection

@section('customjs')
<script src="{{ asset('stisla/assets/modules/select2/dist/js/select2.js')}}"></script>

<script>
    actControl("filter");
    actControl("data");
    
    $('.select2').select2({
            placeholder: "Select a state",
            theme: "bootstrap4"
    });
    
    function actControl(x, y, z) {
        if (x == "data") {
            $("#data-display").load('{{url("spending/data")}}');
        } else if (x == "filter") {
            $("#filter-display").load('{{url("spending/filter")}}');
        } else if( x == "form"){
            $("#modal-setup").attr("class", "modal-dialog modal-lg");
            $("#myLargeModalBody").load('{{url("spending/form")}}');
        }else if (x == "delete") {
            var csrf_token = $('meta[name="csrf-token"]').attr('content');
            swal({
                    title: 'Are you sure?',
                    text: 'Once deleted, you will not be able to recover this imaginary file!',
                    icon: 'warning',
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: 'categories/' + y,
                            type : "POST",
                            data : {'_method' : 'DELETE', '_token' : csrf_token},
                            success:function(data){
                                swal('Poof! Your imaginary file has been deleted!', {
                                icon: 'success',
                                });
                                actControl("data");
                            }
                        });
                    } else {
                        swal('Your imaginary file is safe!');
                    }
                });
        }
    }
</script>
@endsection