<div class="card">
    <div class="card-header">
        <button class="btn btn-primary" role="button" data-toggle="modal" data-target=".bs-modal-lg" style="cursor:pointer" onclick="actControl('form')">Tambah Pengeluaran</button>
    </div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-striped table-md">
                <tbody>
                    <tr>
                        <th>#</th>
                        <th>Kode</th>
                        <th>Tanggal Pengeluaran</th>
                        <th>Penanggungjawab</th>
                        <th>Total Pengeluaran</th>
                        <th>Action</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>PGLR23234</td>
                        <td>{{indo_date("2017-01-09",true)}}</td>
                        <td>Bambang pamungkas</td>
                        <td>Rp. 300000</td>
                        <td><a href="#" class="btn btn-secondary">Detail</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer text-right">
        <nav class="d-inline-block">
            <ul class="pagination mb-0">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1"><i class="fas fa-chevron-left"></i></a>
                </li>
                <li class="page-item active"><a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">2</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#"><i class="fas fa-chevron-right"></i></a>
                </li>
            </ul>
        </nav>
    </div>
</div>
