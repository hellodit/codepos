@extends('layouts.dashboard')
@section('title', 'Cashier')

@section('content')
<div class="card card-secondary">
    <div class="card-header">
        <h4>Cashier</h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text alert alert-info">Nomor Meja / Nama Pelangan</div>
                        </div>
                        <input type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="col-md-1 offset-7">
                <h3>Hallo</h3>
                <p>{{Auth()->user()->first_name.' '.Auth()->user()->last_name}}</p>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Menu</th>
                    <th>Harga</th>
                    <th>Jumlah</th>
                    <th>Total</th>
                    <th>keterangan</th>
                    <th><i class="fas fa-gear"></i></th>
                </tr>
            </thead>
            <tbody class="detail_tambahan"></tbody>
        </table>
        <div class="row">
            <div class="col-md-4">
                <button class="btn btn-primary btn-block" role="button" onclick="addso_row(this)"><i
                        class="fas fa-plus"></i></button>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <b>Total Order</b>
                        <hr>
                        <h1 style="margin:0px; padding:0px"><b><sup><small>Rp </small></sup><span
                                    class="total-display">0</span></b></h1>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for=""><b>Diskon</b></label>
                            <select class="form-control">
                                <option>Option 1</option>
                                <option>Option 2</option>
                                <option>Option 3</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Total Tagihan</b></label>
                            <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp</span>
                                    </div>
                                    <input type="text" class="form-control" name="price" onkeyup="actControl('currency', this)" placeholder="Masukan Harga Bahan ..." value="" disabled>
                                </div>  
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Total Tagihan</b></label>
                            <select class="form-control">
                                <option>Option 1</option>
                                <option>Option 2</option>
                                <option>Option 3</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><b>Jumlah Bayar</b></label>
                            <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp</span>
                                    </div>
                                    <input type="text" class="form-control" name="price" onkeyup="actControl('currency', this)" placeholder="Masukan Harga Bahan ..." value="" >
                                </div>                        
                            </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Kembali</b></label>
                            <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp</span>
                                    </div>
                                    <input type="text" class="form-control" name="price" onkeyup="actControl('currency', this)" placeholder="Masukan Harga Bahan ..." value="" disabled>
                                </div>                        
                            </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Kekurangan</b></label>
                            <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp</span>
                                    </div>
                                    <input type="text" class="form-control" name="price" onkeyup="actControl('currency', this)" placeholder="Masukan Harga Bahan ..." value="" disabled>
                                </div>                        
                            </div>
                    </div>
                    <div class="col-md-6">
                            <button type="submit" class="btn btn-danger btn-block">Batal</button>
                    </div>
                        <div class="col-md-6">
                                <button type="submit" class="btn btn-primary btn-block">Bayar</button>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('customjs')
<script>
    addso_row();

    function addso_row() {
        $(".add-new");
        $(".row_out");
        for (var a = $(".row_tam").length + 1, b = $(".row_out").length + 1; b >= a; b--) {
            var d = b + 1;
            $(".nom_" + b).html(d);
            $(".nom_" + b).attr("class", "nom_" + d);
            $("#menu_select_" + b).attr("id", "menu_select_" + d);
            $("#btn_" + b).attr("onclick", "cut(this, " + d + ")");
            $("#menu_" + b).attr("onchange", "menusa(this," + d + ")");
            $("#menu_" + b).attr("id", "menu_" + d);
            $("#btn_" + b).attr("id", "btn_" + d);
            $(".total_" + b).attr("class", "text-right total_" + d);
            $("#total_add_" + b).attr("id", "total_add_" + d);
            $("#price_" + b).attr("onkeyup", "calcPay(this, " + d + ")");
            $("#price_" + b).attr("id", "price_" + d);
            $("#disc_" + b).attr("onkeyup", "calcPay(this, " + d + ")");
            $("#disc_" + b).attr("id", "disc_" + d);
            $("#compliment_" + b).attr("id", "compliment_" + d);
            $("#qty_" + b).attr("onkeyup", "calcPay(this, " + d + ")");
            $("#qty_" + b).attr("id", "qty_" + d);
            $("#note_" + b).attr("id", "note_" + d);
        }
        b = '<tr class="row_out row_tam add-new">' +
            '<td class="text-center"><p style="margin-top: 5px" class="nom_' + a + '">' + a +
            '</p></td><td id="menu_select_' + a + '"><select class="form-control"><option>Option 1</option><option>Option 2</option></select></div>' +
            '<td><div class="input-group"><input type="text" readonly name="harga_add[]" onkeyup="calcPay(this, ' + a + ')" id="price_' + a + '" value="" class="form-control" placeholder="Harga"></div></td>' +
            '<td><input type="text" onkeyup="calcPay(this, ' + a + ')" name="qty_menu[]" id="qty_' + a + '" value="1" tabindex="2" class="form-control" placeholder="Qty"></td>' +
            '<td class="text-right total_' + a + '" style="vertical-align:middle">Rp 0</td>' + '<td>' +
            '<input type="text" id="note_' + a + '" name="note[]" value="" class="form-control" placeholder="Note. Maks 30 Karakter" tabindex="2">' +
            '</td>' + '<td class="text-right"><input type="hidden" name="compliment[]" id="compliment_' + a + '" value="1"><input type="hidden" name="total_add[]" id="total_add_' + a + '" value="" class="form-control"><center><button type="button" class="btn btn-sm btn-danger" id="btn_' + a + '" onclick="cut(this, ' + a + ')"><i class="fa fa-times"></i></button></center></td>' +
            '</tr>';
        $(".detail_tambahan").append(b);
    }

    function cut(a, b) {
        for (var d = $(".row_out").length, c = b - 1; c < d; c++) {
            var e = c + 1;
            $(".nom_" + e).html(c);
            $(".nom_" + e).attr("class", "nom_" + c);
            $("#menu_select_" + e).attr("id", "menu_select_" + c);
            $("#btn_" + e).attr("onclick", "cut(this, " + c + ")");
            $("#menu_" + e).attr("onchange", "menusa(this," + c + ")");
            $("#menu_" + e).attr("id", "menu_" + c);
            $("#btn_" + e).attr("id", "btn_" + c);
            $(".total_" + e).attr("class", "text-right total_" + c);
            $("#total_add_" + e).attr("id", "total_add_" + c);
            $("#price_" + e).attr("onkeyup", "calcPay(this, " + c + ")");
            $("#price_" + e).attr("id", "price_" + c);
            $("#disc_" + e).attr("onkeyup", "calcPay(this, " + c + ")");
            $("#disc_" + e).attr("id", "disc_" + c);
            $("#compliment_" + e).attr("id", "compliment_" + c);
            $("#qty_" + e).attr("onkeyup", "calcPay(this, " + c + ")");
            $("#qty_" + e).attr("id", "qty_" + c);
            $("#note_" + e).attr("id", "note_" + c);
        }
        d = a.parentNode.parentNode.parentNode;
        d.parentNode.removeChild(d);
        $("#orders_id").val();
        calcPayNew();
    }
</script>

@endsection