<form action="">
    @csrf

    <div class="form-group">
        <label for="labeName">Name*</label>
        <input type="text" class="form-control">
    </div>

    <div class="form-group">
        <label>Start*</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-clock"></i>
                </div>
            </div>
            <input type="text" class="form-control timepicker">
        </div>
    </div>

    <div class="form-group">
        <label>End*</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-clock"></i>
                </div>
            </div>
            <input type="text" class="form-control timepicker">
        </div>
    </div>

    <input type="button" value="submit" class="btn btn-primary">
</form>