<div class="card">
        <div class="card-header">
                <h4>List Shift</h4>
                <a href="#" class="btn btn-primary float-left" role="button" data-toggle="modal" data-target=".bs-modal-lg" style="cursor:pointer" onclick="actControl('addShift')">Tambah data</a>
        </div>

        <div class="card-body p-0">
            <div class="table-responsive">
                <table class="table table-striped table-md">
                    <tbody>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Start</th>
                            <th>End</th>
                            <th>Action</th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Shift 1</td>
                            <td>12:00</td>
                            <td>15:00</td>
                            <td>
                                <a href="#" class="btn btn-info"><i class="fas fa-edit"></i></a>
                                <a href="#" class="btn btn-danger" onclick="actControl('delete')"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Shift 2</td>
                            <td>15:00</td>
                            <td>20:00</td>
                            <td>
                                <a href="#" class="btn btn-info"><i class="fas fa-edit"></i></a>
                                <a href="#" class="btn btn-danger" onclick="actControl('delete')"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer">
            <p class="text-muted">Ini adalah catatan untuk balik</p>
        </div>
    </div>