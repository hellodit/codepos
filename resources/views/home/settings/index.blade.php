@extends('layouts.dashboard')
@section('title')
Setting
@endsection

@section('content')
<div class="section">
    <div class="section-header">
        <h1>General Settings</h1>
        @include('home.parts.breadcrumb')
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="card">
          <div class="card-header">
            <h4>Jump To</h4>
          </div>
          <div class="card-body">
            <ul class="nav nav-pills flex-column">
              <li class="nav-item"><a href="#" class="nav-link" onclick="actControl('basic')">General</a></li>
              <li class="nav-item"><a href="#" class="nav-link" onclick="actControl('shift')">Shift</a></li>
            </ul>
          </div>
        </div>
      </div>

      <div class="col-md-8">
          <div id="settingcontent"></div>
      </div>
</div>
@endsection

@section('customjs')
  <script src="{{asset('assets/modules/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

    <script>
        actControl("basic");

        function actControl(x, y, z) {
            if (x == "basic") {
                $("#settingcontent").load('{{url("basic-setting")}}');
            } else if (x == "shift") {
                $("#settingcontent").load('{{url("shift-setting")}}');
            } else if( x == "addShift"){
                $("#modal-setup").attr("class", "modal-dialog modal-md");
                $("#myLargeModalBody").load('{{url("form-shift")}}');
            }
        }
    </script>
@endsection