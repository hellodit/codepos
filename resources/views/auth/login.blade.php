@extends('layouts.auth')
@section('title')
Login
@endsection
@section('content')
<section class="section">
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                <div class="login-brand">
                    {{-- <img src="" alt="logo" width="100" class="shadow-light rounded-circle"> --}}
                </div>
                <div class="card card-primary">
                    <div class="card-header">
                        <h4>Login</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            @if(session()->has('login_error'))
                                <div class="alert alert-danger">
                                    {{ session()->get('login_error') }}
                                </div>
                            @endif

                            <div class="form-group">
                                <label for="email">Email / username</label>
                                <input id="identity" type="identity" class="form-control  {{ $errors->has('identity') ? ' is-invalid' : '' }}"
                                    name="identity" value="{{ old('identity') }}" tabindex="1" >
                                @if ($errors->has('identity'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('identity') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="d-block">
                                    <label for="password" class="control-label">Password</label>
                                    <div class="float-right">
                                        @if (Route::has('password.request'))
                                        <a class="text-small" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                        @endif
                                    </div>
                                </div>
                                <input id="password" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                    name="password" tabindex="2">
                                @if ($errors->has('password'))
                                <div class="invalid-feedback">
                                    <strong>{{$errors->first('password')}}</strong>
                                </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember-me"
                                        {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="remember-me">{{ __('Remember Me') }}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="simple-footer">
                    Copyright &copy; Codepos 2018
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
