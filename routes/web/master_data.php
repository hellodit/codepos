<?php



// user router
Route::get('users/form/{id?}', 'UserController@form');
Route::get('users/data', 'UserController@data');

// ingredient routes
Route::get('ingredients/form/{id?}', 'IngredientController@form');
Route::get('ingredients/data', 'IngredientController@data');
Route::get('ingredients/ajax', 'IngredientController@AjaxIngredients');


//category routes
Route::get('categories/form/{id?}', 'CategoryController@form');
Route::get('categories/data', 'CategoryController@data');
Route::get('categories/ajax', 'CategoryController@AjaxCategories');

Route::resource("users","UserController");
Route::post("users/{id}","UserController@update");
Route::resource('ingredients', 'IngredientController');
Route::resource('categories', 'CategoryController');
//Route::resource("menus","MenuController");


?>