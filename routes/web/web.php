<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');


Route::get('/', function () {
    return view('auth.login');
});

Route::get('/logout', function(){
    Auth::logout();
    return Redirect::to('login');
 });

 Route::get('/home', 'HomeController@index')->name('home');

 Auth::routes(['register' => false]);

Route::get('settings','SettingController@index');
Route::view('basic-setting','home.settings.basic-setting');
Route::view('shift-setting','home.settings.shift-setting');
Route::view('form-shift','home.settings.form-shift');

Route::get('menus','MenuController@index');
Route::view('menus/form', 'home.master_data.menus.form');
Route::view('menus/filter', 'home.master_data.menus.filter');
Route::view('menus/data', 'home.master_data.menus.data');
