
<?php

Route::view('cashier', 'home.manage.cashier');
Route::get('spending', 'SpendingController@index');
Route::view('spending/data', 'home.manage.spending.data');
Route::view('spending/filter', 'home.manage.spending.filter');
Route::view('spending/form', 'home.manage.spending.form');

?>