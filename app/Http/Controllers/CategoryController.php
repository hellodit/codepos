<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Models\Category;
use Validator;
use Response;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected $rules = [
        'name' => 'required',
        'type' => 'required'
    ];

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modalSetup = true;
        return view('home.master_data.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules);

        if($validator->fails()){
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toArray()
            ));
        }else{
            $category = new \App\Models\Category();
            $category->name = $request->get('name');
            $category->type = $request->get('type');
            $category->save();
            return response()->json($category);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(empty($id)){
            return response()->json([
                'status' => 0,
                'pesan' => 'Kategori tidak ditemukan!!'
            ]);
        }

        $category = Category::find($id);
        $category->name = $request->get('name');
        $category->type = $request->get('type');
        $category->update();

        return response()->json([
            'status' => 1,
            'pesan' => 'Kategori baru telah berhasil diperbarui!!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = \App\Models\Category::findOrFail($id);
        $categories->delete();
        return response()->json($categories);
    }

    public function data(){
        $categories = Category::paginate(10);
        return view('home.master_data.categories.data',['categories' => $categories]);
    }

    public function form($id = null){
        $data['act'] = (empty($id) ? "Tambah Data" : "Edit Data");
        $data['url'] = (empty($id) ? url('categories') : url('categories/'.$id));
        $data['action'] = (empty($id) ? "POST" : "PUT");
        $data['category'] = Category::find($id);
        return view('home.master_data.categories.form',['data' => $data]);
    }

    public function AjaxCategories() {
        
        $sqlwhere = null;
        $categories = Category::all();
        $data = "<option value=''>" . (!empty($_GET['display']) ? $_GET['display'] : "Pilih kategori") . "</option>";

        foreach ($categories as $key => $category) {
            $data .= "<option value='{$category->id}'" . (!empty($_GET['selected']) ? ($_GET['selected'] == $category->id ? "selected" : null) : null) ."> {$category->name} </option>";
        }

        return array('id' => $_GET['alias'], 'data' => $data, "display" => (!empty($_GET['display']) ? $_GET['display'] : ""));
    }
}
