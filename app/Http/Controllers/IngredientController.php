<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Models\Ingredient;
use Validator;
use Response;

class IngredientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected $rules = [
        'name' => 'required',
        'unit' => 'required',
        'price' => 'required'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home.master_data.ingredients.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules);

        if($validator->fails()){
            return Response::json(array(
                'errors' => $validator->getMessageBag()->toArray()
            ));
        }else{
            $ingredient = new \App\Models\Ingredient();
            $ingredient->name = $request->get('name');
            $ingredient->unit = $request->get('unit');
            $ingredient->price = str_replace('.','',$request->get('price'));
            $ingredient->save();
            return response()->json($ingredient);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(empty($id)){
            return response()->json([
                'status' => 0,
                'pesan' => 'Bahan tidak ditemukan!'
            ]);
        }

        $ingredient = Ingredient::find($id);
        $ingredient->name = $request->get('name');
        $ingredient->unit = $request->get('unit');
        $ingredient->price = $request->get('price');
        $ingredient->update();

        return response()->json([
            'status' => 1,
            'pesan' => 'Bahan berhasil ditambahkan'
        ]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ingredients = \App\Models\Ingredient::findOrFail($id);
        $ingredients->delete();
        return response()->json($ingredients);
    }

    public function data(){
        $ingredients = Ingredient::paginate(10);
        return view('home.master_data.ingredients.data',['ingredients' => $ingredients]);
    }

    public function form($id = null){
        $data['act'] = (empty($id) ? "Tambah Data" : "Edit Data");
        $data['url'] = (empty($id) ? url('ingredients') : url('ingredients/'.$id));
        $data['action'] = (empty($id) ? "POST" : "PUT");
        $data['ingredient'] = Ingredient::find($id);
        return view('home.master_data.ingredients.form',['data' => $data]);
    }

    public function AjaxIngredients() {
        $sqlwhere = null;
        $categories = Ingredient::all();
        $data = "<option value=''>" . (!empty($_GET['display']) ? $_GET['display'] : "Pilih kategori") . "</option>";

        foreach ($categories as $key => $category) {
            $data .= "<option value='{$category->id}'" . (!empty($_GET['selected']) ? ($_GET['selected'] == $category->id ? "selected" : null) : null) ."> {$category->name} </option>";
        }

        return array('id' => $_GET['alias'], 'data' => $data, "display" => (!empty($_GET['display']) ? $_GET['display'] : ""));
    }
}
