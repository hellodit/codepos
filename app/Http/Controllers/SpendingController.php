<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SpendingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $modalSetup = true;
        return view('home.manage.spending.index',['modal' => $modalSetup]);
    }
}
