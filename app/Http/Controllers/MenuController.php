<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $modalSetup = true;
        return view('home.master_data.menus.index', ['modal' => $modalSetup]);
    }
}
