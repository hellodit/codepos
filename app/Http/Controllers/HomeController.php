<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Category;
use App\Models\Menu;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::all()->count();
        $menus = Menu::all()->count();
        $categories = Category::all()->count();

        $count = [
            'users' => $users,
            'menus' => $menus,
            'categories' => $categories
        ];
         
        return view('home/index', ['count'=> $count]);
    }
}
