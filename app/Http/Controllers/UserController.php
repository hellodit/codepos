<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\User;
use App\Mail\WelcomeMsg;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use File;
use Image;
use Validator;
use Response;

class Usercontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected $rules = [
        'username' => 'required',
        'email' => 'required',
        'first_name' => 'required',
        'last_name' => 'required',
        'level' => 'required',
        'shift' => 'required'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modalSetup = true;
        return view('home.master_data.users.index',['modal' => $modalSetup]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $validator = Validator::make(Input::all(), $this->rules);

        if($validator->fails()){
            return Response::json(array(
                'status' => 0,
                'pesan' => 'Silahkan isi semua bidang bertanda bintang wajib!!'
            ));
        }else{
            $user = new \App\Models\User();
            
            if($request->file('avatar')){
                $this->path = storage_path('app/public/avatar');
                $this->dimensions = ['300', '60'];
    
                if (!File::isDirectory($this->path)) {
                    File::makeDirectory($this->path);
                }

                $file = $request->file('avatar');
                $fileName = Carbon::now()->timestamp . '_' .Auth()->user()->username. '.' . $file->getClientOriginalExtension();
                Image::make($file)->save($this->path . '/' . $fileName);
    
                foreach ($this->dimensions as $row) {
                    $canvas = Image::canvas($row, $row);
                    $resizeImage  = Image::make($file)->resize($row, $row, function($constraint) {
                        $constraint->aspectRatio();
                    });
                    
                    if (!File::isDirectory($this->path . '/' . $row)) {
                        File::makeDirectory($this->path . '/' . $row);
                    }
    
                    $canvas->insert($resizeImage, 'center');
                    $canvas->save($this->path . '/' . $row . '/' . $fileName);
                }
    
                $user->photo = $fileName;
                
            }else{
                $user->photo = 'default.jpg';
            }

            $user->username = $request->get('username');
            $user->email = $request->get('email');
            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->level = $request->get('level');
            $user->shift_id = $request->get('shift');
            $user->password = \bcrypt('123456');
            $user->status = '2';
            $user->save();
            // Mail::to($request->get('email'))->send(new WelcomeMsg());
            
            return response()->json([
                    'status' => 1,
                    'pesan' => 'Akun baru telah berhasil dibuat'
                ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['user'] = User::find($id);
        return view('home.master_data.users.detail',['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (empty($id)){
            return response()->json([
                'status' => 0,
                'pesan' => 'User tidak ditemukan!!'
            ]);
        }
        


        $user = User::find($id);
        $user->username = $request->get('username');
        $user->email = $request->get('email');
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->level = $request->get('level');
        $user->shift_id = $request->get('shift');
        $user->password = \bcrypt('123456');

        if($request->file('avatar')){

            $this->path = storage_path('app/public/avatar');
            $this->dimensions = ['60', '300'];
            
            $file = $request->file('avatar');
            $fileName = Carbon::now()->timestamp . '_' .Auth()->user()->username. '.' . $file->getClientOriginalExtension();
            Image::make($file)->save($this->path . '/' . $fileName);

            foreach ($this->dimensions as $row) {
                $canvas = Image::canvas($row, $row);
                $resizeImage  = Image::make($file)->resize($row, $row, function($constraint) {
                    $constraint->aspectRatio();
                });
                
                if (!File::isDirectory($this->path . '/' . $row)) {
                    File::makeDirectory($this->path . '/' . $row);
                }

                $canvas->insert($resizeImage, 'center');
                $canvas->save($this->path . '/' . $row . '/' . $fileName);
            }

            $user->photo = $fileName;
            
        }else{
            $user->photo = 'default.jpg';
        }

        $user->update();

        return response()->json([
                    'status' => 1,
                    'pesan' => 'Akun baru telah berhasil diperbarui!!'
                ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currentid = Auth()->user()->id;
        $currentlvl = Auth()->user()->level;

        if($currentid == $id){
            return response()->json([
                'status' => 0,
                'pesan' => 'Tidak dapat menghapus akun sendiri!'
            ]);
        }else if($currentlvl == 2){
            return response()->json([
                'status' => 0,
                'pesan' => 'Hanya admin yang dapat melakukan hapus akun!'
            ]);
        }


        $path = User::find($id)->photo;
        Storage::delete('public/avatar/300/'.$path);
        Storage::delete('public/avatar/60/'.$path);
        Storage::delete('public/avatar/'.$path);

        $user = User::findOrFail($id);
        $user->delete();

        return response()->json([
            'status' => 1,
            'pesan' => 'Data telah berhasil dihapus'
        ]);

    }

    public function data(){
        $users = User::paginate(5);
        return view('home.master_data.users.data',['users' => $users]);
    }

    public function form($id = null){
        $data['act'] = (empty($id) ? "Tambah Data" : "Edit Data");
        $data['url'] = (empty($id) ? url('users') : url('users/'.$id));
        // $data['ajaxtype'] = (empty($id) ? "POST" : "PUT");
        $data['user'] = User::find($id);
        return view('home.master_data.users.form',['data' => $data]);
    }
    
}
