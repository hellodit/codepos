<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    public function menus(){
        return $this->belongsToMany("App\Models\Menu");
    }
}
