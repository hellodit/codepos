<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    // public function users(){
    //     return $this->belongsTo("App\User");
    // }

    public function settings(){
        return $this->belongsTo("App\Models\Setting");
    }
}
