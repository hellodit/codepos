<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'admin@gmail.com',
            'username' => 'admin',
            'password' => bcrypt('admin'),
            'first_name' => 'Admin',
            'last_name' => 'Codepost',
            'level' => 1,
            'status' => 1,
            'photo' => 'avatar/default.jpg',
            'shift_id' => 1
        ]);
    }
}
